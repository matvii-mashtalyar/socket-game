import { users, rooms } from '../data';
import onCreateRoom from './onCreateRoom';
import { mapToJson, jsonToMap } from './helper';
import leaveRoom from './leaveRoom';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from './config';

export default io => {
  io.on("connection", socket => {

    const username = socket.handshake.query.username;

    socket.emit("UPDATE_USERS", mapToJson(users));
    socket.emit("UPDATE_ROOMS", mapToJson(rooms));
    users.set(username, socket.id);

    onCreateRoom(io, socket);

    socket.on('JOIN_ROOM', roomId => {
      const roomData = rooms.get(roomId);
      if (roomData.roomCountUsers >= MAXIMUM_USERS_FOR_ONE_ROOM) {
        return;
      }
      roomData.roomCountUsers++;
      roomData.users.set(username, { ready: false });

      rooms.set(roomId, roomData);

      const activeRoom = rooms.get(roomId);
      activeRoom.users = mapToJson(activeRoom.users);

      socket.join(roomId);
      io.to(roomId).emit('JOIN_ROOM_DONE', { roomData: rooms.get(roomId) });
      io.emit('UPDATE_ROOMS', mapToJson(rooms));

      activeRoom.users = jsonToMap(activeRoom.users);
    })

    socket.on("LEAVE_ROOM", () => {
      leaveRoom(io, socket);
      socket.emit("LEAVE_ROOM_DONE");
    })

    socket.on("READY", () => {
      const roomId = Object.keys(socket.rooms)[1];
      const roomData = rooms.get(roomId);
      const user = roomData.users.get(username);
      user.ready = true;
      io.to(roomId).emit("READY", mapToJson(roomData.users));
    })

    socket.on("NOT READY", () => {
      const roomId = Object.keys(socket.rooms)[1];
      const roomData = rooms.get(roomId);
      const user = roomData.users.get(username);
      user.ready = false;
      io.to(roomId).emit("READY", mapToJson(roomData.users));
    })

    socket.on("disconnecting", () => {
      leaveRoom(io, socket);
    });

    socket.on("disconnect", () => {
      users.delete(username);
    })
  });
}