import { rooms } from '../data';
import { mapToJson, jsonToMap } from './helper';
import { v4 as uuidv4 } from 'uuid';

export default (io, socket) => {
  socket.on("CREATE_ROOM", roomName => {

    if (!roomName) {
      return;
    }

    // Check if room exists
    let roomExists;
    rooms.forEach(value => {
      if (value.roomName === roomName) {
        roomExists = true;
      }
    })

    if (roomExists) {
      return;
    }

    const roomId = uuidv4();
    rooms.set(roomId,
      {
        roomName: roomName,
        roomCountUsers: 1,
        users: new Map([
          [socket.handshake.query.username, { ready: false }]
        ])
      });

    const activeRoom = rooms.get(roomId);
    activeRoom.users = mapToJson(activeRoom.users);

    socket.join(roomId);
    socket.emit('JOIN_ROOM_DONE', { roomData: activeRoom });
    io.emit('UPDATE_ROOMS', mapToJson(rooms));

    activeRoom.users = jsonToMap(activeRoom.users);
  })
}
