import { rooms } from "../data";
import { mapToJson, jsonToMap } from "./helper";

export default (io, socket) => {
  const roomId = Object.keys(socket.rooms)[1];

  if (!roomId) {
    return;
  }

  const roomData = rooms.get(roomId);
  roomData.roomCountUsers--;
  roomData.users.delete(socket.handshake.query.username);

  rooms.set(roomId, roomData);

  if (roomData.roomCountUsers < 1) {
    socket.leave(roomId);
    rooms.delete(roomId);
    socket.emit("LEAVE_ROOM_DONE");
    io.emit('UPDATE_ROOMS', mapToJson(rooms));
    return;
  }

  const activeRoom = rooms.get(roomId);
  activeRoom.users = mapToJson(activeRoom.users);

  socket.leave(roomId);
  socket.emit("LEAVE_ROOM_DONE");
  socket.to(roomId).emit("UPDATE_ROOM", { roomData: rooms.get(roomId) });
  io.emit('UPDATE_ROOMS', mapToJson(rooms));

  activeRoom.users = jsonToMap(activeRoom.users);
}