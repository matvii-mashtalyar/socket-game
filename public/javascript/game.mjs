import { createElement, addClass, removeClass } from "./helper.mjs";

function jsonToMap(jsonStr) {
  return new Map(JSON.parse(jsonStr));
}

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("/game", { query: { username } });

const addRoomBtn = document.getElementById("add-room-btn");
const roomsContainer = document.getElementById("rooms-container");
const gamePage = document.getElementById("game-page");
const quitRoomButton = document.getElementById("quit-room-btn");
const roomGameName = document.getElementById("room-name");
const playersContainer = document.getElementById("players-container");
const readyButton = document.getElementById("ready-btn");

const validateUser = json => {
  const users = jsonToMap(json);
  if (users.get(username)) {
    sessionStorage.clear();
    window.location.replace("/login");
    alert(`User ${username} already exists`);
  }
}

const createRoom = (id, { roomName, roomCountUsers }) => {

  const usersCount = document.createElement("p");
  usersCount.innerText = `${roomCountUsers} users connected`;

  const name = createElement({
    tagName: "h3",
    className: "mb-5",
  });
  name.innerText = `${roomName}`;

  const joinRoomButton = createElement({
    tagName: "button",
    className: "w-100 btn btn-primary join-btn"
  });
  joinRoomButton.innerText = "Join";

  const room = createElement({
    tagName: "div",
    className: "room me-4 p-3 rounded-3 border border-dark",
    attributes: { id: id }
  });

  room.append(...[usersCount, name, joinRoomButton])

  const onJoinRoom = () => {
    socket.emit("JOIN_ROOM", id);
  };

  joinRoomButton.addEventListener("click", onJoinRoom);

  return room;
};

const updateRooms = json => {
  const allRooms = [];
  const rooms = jsonToMap(json);
  rooms.forEach((data, id) => {
    allRooms.push(createRoom(id, data));
  });

  roomsContainer.innerHTML = "";
  roomsContainer.append(...allRooms);
};

const createPlayer = (name, ready) => {
  const userName = createElement({
    tagName: "span"
  })
  userName.innerText = name;
  if (name === username) {
    userName.innerText = `${name}(you)`;
  }
  const userTop = createElement({ tagName: "div" });
  userTop.append(
    createElement({
      tagName: "div",
      className: `${ready ? 'ready-status-green' : 'ready-status-red'} ready-status d-inline-block`
    }),
    userName
  )
  const userBottom = createElement({
    tagName: "div",
    className: `user-progress-${name} border border-dark h-20`,
    attributes: { style: "height: 20px;" }
  })
  const user = document.createElement("div")
  user.append(userTop, userBottom);

  return user;
}

const updateReadyButton = user => {
  if (user.ready) {
    readyButton.innerText = "NOT READY";
    return;
  }
  readyButton.innerText = "READY";
}

const updatePlayers = data => {

  let users = [];
  data.forEach((userData, userName) => {
    users.push(createPlayer(userName, userData.ready));
  })

  playersContainer.innerHTML = "";
  playersContainer.append(...users);
}

addRoomBtn.addEventListener('click', () => {
  socket.emit('CREATE_ROOM', prompt('Input room name'));
})

quitRoomButton.addEventListener('click', () => {
  socket.emit("LEAVE_ROOM");
})

readyButton.addEventListener('click', (e) => {
  if (e.target.innerText === "READY") {
    socket.emit("READY");
    return;
  }
  if (e.target.innerText === "NOT READY") {
    socket.emit("NOT READY");
    return;
  }
})

socket.on('UPDATE_USERS', validateUser)
socket.on('UPDATE_ROOMS', updateRooms)
socket.on('JOIN_ROOM_DONE', ({ roomData }) => {
  const users = jsonToMap(roomData.users);
  removeClass(gamePage, 'd-none');
  updatePlayers(users);

  roomGameName.innerText = roomData.roomName;
})
socket.on("UPDATE_ROOM", ({ roomData }) => {
  const users = jsonToMap(roomData.users);
  updatePlayers(users);
  updateReadyButton(users.get(username));
})
socket.on("LEAVE_ROOM_DONE", () => {
  addClass(gamePage, 'd-none');
})
socket.on("READY", json => {
  const users = jsonToMap(json);
  updatePlayers(users);
  updateReadyButton(users.get(username));
})
socket.on("NOT READY", json => {
  const users = jsonToMap(json);
  updatePlayers(users);
  updateReadyButton(users.get(username));
})

